System requirements:\
OS: ubuntu or other python compatible\
Python >= 3.6\
Pipenv (optional)\
Install:
1. Install dependencies: pipenv sync

 - use environment variable PIPENV_VENV_IN_PROJECT=1 to create separate .venv for project:\
export PIPENV_VENV_IN_PROJECT=1

2. or install dependencies with pip:
 - pip install numpy pyqt5

Usage:
1. Go to project root
2. (only if you are using pipenv) launch pipenv: pipenv shell
3. Launch gui.py: \
A.	Ubuntu: 
python3 gui.py \
B.	Windows: 
python gui.py
4. Set required settings and press start button
5. Wait until finish
6. Open directory /generated and check created maps.
7. Open map in gazebo: gazebo \<name of world>

Generation settings:
1. Generation Type: type of generated RSE normal diagonal – one diagonal obstacle, it is most relevant type, random and normal random is just a random mess of blocks, normal random is softer
2. Maps count: count of generated maps
3. Size in chunks: size of map in chunks. Example: Size in chunks 2 and Chunk size 10 will give you a RSE 20x20 (or 23x23 with borders) 
4. Chunk division: create border between chunks or not
5. Chunk size: size of one chunk in blocks
6. Max height: height of generated obstacles in height steps
7. Softness: from 0 to 100, bigger number – closer to flat environment, 0 generates a vertical obstacle.
8. Height Step: height of one height step
