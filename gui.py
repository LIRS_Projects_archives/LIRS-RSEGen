# -*- coding: utf-8 -*-

import sys
from threading import Thread

from PyQt5 import QtCore, QtWidgets

from rsegen import Generator


class SliderLabelBox(QtWidgets.QHBoxLayout):
    """
    Custom slider
    """
    def __init__(self, min, current, max):
        super().__init__()
        self.addStretch(20)
        self.addWidget(min)
        self.addStretch(1200)
        self.addWidget(current)
        self.addStretch(1200)
        self.addWidget(max)


class MyLabel(QtWidgets.QLabel):
    """
    Custom label
    """
    def __init__(self, *args):
        super().__init__(*args)
        second_font = self.font()
        second_font.setPointSize(9)
        self.setFont(second_font)
        self.setStyleSheet("color: rgb(80,80,80)")


class Ui_MainWindow(QtWidgets.QMainWindow):
    """
    Main UI class
    """

    def __init__(self):
        """
        Init global UI Widgets
        """
        super().__init__()
        self.left_vbox_down = QtWidgets.QVBoxLayout()
        self.table = QtWidgets.QTableWidget(self)
        self.apply_button = QtWidgets.QPushButton(self)
        self.remove_button = QtWidgets.QPushButton(self)
        self.clear_button = QtWidgets.QPushButton(self)
        self.centralwidget = QtWidgets.QWidget(self)
        self.right_first = QtWidgets.QSlider(QtCore.Qt.Horizontal, self)
        self.right_first.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self.right_first.setTickInterval(1)
        self.right_first.valueChanged[int].connect(self.set_right_first_value)
        self.right_first_label_min = MyLabel(self)
        self.right_first_label_min.setText('1')
        self.right_first_label_current = MyLabel(self)
        self.right_first_label_max = MyLabel(self)
        self.right_first_label_box = SliderLabelBox(self.right_first_label_min,
                                                    self.right_first_label_current,
                                                    self.right_first_label_max)
        self.right_second = QtWidgets.QSlider(QtCore.Qt.Horizontal, self)
        self.right_second.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self.right_second.setTickInterval(1)
        self.right_second.valueChanged[int].connect(self.set_right_second_value)
        self.right_second_label_min = MyLabel(self)
        self.right_second_label_min.setText('1')
        self.right_second_label_current = MyLabel(self)
        self.right_second_label_max = MyLabel(self)
        self.right_second_label_box = SliderLabelBox(self.right_second_label_min,
                                                     self.right_second_label_current,
                                                     self.right_second_label_max)
        self.left_first = QtWidgets.QSlider(QtCore.Qt.Horizontal, self)
        self.left_first.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self.left_first.setTickInterval(1)
        self.left_first.valueChanged[int].connect(self.set_left_first_value)
        self.left_first_label_min = MyLabel(self)
        self.left_first_label_min.setText('1')
        self.left_first_label_current = MyLabel(self)
        self.left_first_label_max = MyLabel(self)
        self.left_first_label_box = SliderLabelBox(self.left_first_label_min,
                                                   self.left_first_label_current,
                                                   self.left_first_label_max)
        self.left_second = QtWidgets.QSlider(QtCore.Qt.Horizontal, self)
        self.left_second.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self.left_second.setTickInterval(1)
        self.left_second.valueChanged[int].connect(self.set_left_second_value)
        self.left_second_label_min = MyLabel(self)
        self.left_second_label_min.setText('1')
        self.left_second_label_current = MyLabel(self)
        self.left_second_label_max = MyLabel(self)
        self.left_second_label_box = SliderLabelBox(self.left_second_label_min,
                                                    self.left_second_label_current,
                                                    self.left_second_label_max)
        self.labelr_4 = QtWidgets.QLabel()
        self.labelr_3 = QtWidgets.QLabel()
        self.labelr_2 = QtWidgets.QLabel()
        self.labelr = QtWidgets.QLabel()
        self.right_vbox_up = QtWidgets.QVBoxLayout()
        self.right_vbox_down = QtWidgets.QVBoxLayout()
        self.right_hbox_down = QtWidgets.QHBoxLayout()
        self.grid_layout = QtWidgets.QGridLayout()
        self.left_vbox = QtWidgets.QVBoxLayout()
        self.right_vbox_down_left = QtWidgets.QVBoxLayout()
        self.vbox = QtWidgets.QGridLayout()
        self.statusbar = QtWidgets.QStatusBar(self)
        self.menubar = QtWidgets.QMenuBar(self)
        self.help = QtWidgets.QMenu("Help", self)
        self.impAct = QtWidgets.QAction('Help', self)
        self.label_9 = QtWidgets.QLabel(self)
        self.label_8 = QtWidgets.QLabel(self)
        self.label_7 = QtWidgets.QLabel(self)
        self.label_6 = QtWidgets.QLabel(self)
        self.label_5 = QtWidgets.QLabel(self)
        self.label_4 = QtWidgets.QLabel(self)
        self.label_3 = QtWidgets.QLabel(self)
        self.label_2 = QtWidgets.QLabel(self)
        self.label = QtWidgets.QLabel(self)
        self.progressBar = QtWidgets.QProgressBar(self)
        self.start = QtWidgets.QPushButton(self)
        self.modeling_type = QtWidgets.QComboBox(self)
        self.height_step = QtWidgets.QSpinBox(self)
        self.chunk_size = QtWidgets.QSpinBox(self)
        self.divide_type = QtWidgets.QComboBox(self)
        self.gen_type = QtWidgets.QComboBox(self)
        self.softness = QtWidgets.QSpinBox(self)
        self.count_edit = QtWidgets.QSpinBox(self)
        self.strength = QtWidgets.QSpinBox(self)
        self.size_edit = QtWidgets.QSpinBox(self)
        self.obstacles = []
        self.initUI()

    def initUI(self):
        """
        Sets the initial size of the window in pixels setGeometry(last 2 values),
        window title self.setWindowTitle()
        and builds the GUI in self.show() 
        """
        self.setup()
        self.setGeometry(500, 300, 900, 600)
        self.setWindowTitle('LIRS RSE Geneartor')
        self.show()

    def setup(self):
        """
        Interface building
        """
        self.left_vbox.setAlignment(QtCore.Qt.AlignTop)
        self.right_vbox_up.setAlignment(QtCore.Qt.AlignTop)
        left_hbox_down = QtWidgets.QHBoxLayout()

        left_up = QtWidgets.QFrame(self)
        left_up.setFrameShape(QtWidgets.QFrame.StyledPanel)
        left_up.setLayout(self.left_vbox)
        left_up.setMinimumWidth(220)
        left_down = QtWidgets.QFrame(self)
        left_down.setFrameShape(QtWidgets.QFrame.StyledPanel)
        left_down.setLayout(self.left_vbox_down)
        left_down.setMinimumWidth(220)
        self.right_vbox_down_left.addWidget(self.apply_button)
        self.right_vbox_down_left.addWidget(self.remove_button)
        self.right_vbox_down_left.addWidget(self.clear_button)
        right_up = QtWidgets.QFrame(self)
        right_up.setFrameShape(QtWidgets.QFrame.StyledPanel)
        right_up.setLayout(self.right_vbox_up)
        right_up.setMinimumWidth(220)
        right_down = QtWidgets.QFrame(self)
        right_down.setFrameShape(QtWidgets.QFrame.StyledPanel)
        right_down.setLayout(self.right_hbox_down)
        right_down.setMinimumWidth(220)
        right_splitter = QtWidgets.QSplitter(QtCore.Qt.Vertical, self)
        right_splitter.addWidget(right_up)
        right_splitter.addWidget(right_down)
        right_splitter.setCollapsible(0, False)
        right_splitter.setCollapsible(1, False)
        right_splitter.setSizes([right_splitter.size().height() * 0.2, right_splitter.size().height() * 0.8])
        self.right_hbox_down.addLayout(self.right_vbox_down_left)
        self.right_hbox_down.addLayout(self.right_vbox_down)
        self.right_vbox_down.addWidget(self.table)
        self.apply_button.setText("+")
        self.apply_button.setMaximumWidth(36)
        self.apply_button.clicked.connect(self.add_obstacle)
        self.remove_button.setText("-")
        self.remove_button.setMaximumWidth(36)
        self.remove_button.clicked.connect(self.remove_obstacle)
        self.clear_button.setText("C")
        self.clear_button.setMaximumWidth(36)
        self.clear_button.clicked.connect(self.clean_obstacles)
        self.table.setColumnCount(5)
        self.table.setRowCount(1)
        self.table.setHorizontalHeaderLabels(["id", "obstacle type", "height", "softness", "coordinates"])

        self.table.setColumnWidth(0, 30)
        self.table.setColumnWidth(1, 150)

        self.table.horizontalHeaderItem(0).setToolTip("id")
        self.table.horizontalHeaderItem(1).setToolTip("obstacle type")
        self.table.horizontalHeaderItem(2).setToolTip("height")
        self.table.horizontalHeaderItem(3).setToolTip("softness")
        self.table.horizontalHeaderItem(4).setToolTip("obstacle possible coordinates range")

        left_splitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        left_splitter.addWidget(left_up)
        left_splitter.addWidget(left_down)
        left_splitter.setCollapsible(0, False)
        left_splitter.setCollapsible(1, False)

        splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)  # разделение интерфейса на 2 части (таблица и поля)
        splitter.addWidget(left_splitter)
        splitter.addWidget(right_splitter)
        splitter.setCollapsible(0, False)
        splitter.setCollapsible(1, False)
        splitter.setSizes([splitter.size().height() * 0.2, splitter.size().height() * 0.8])

        self.setCentralWidget(self.centralwidget)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(splitter)
        self.centralwidget.setLayout(vbox)
        self.label.setObjectName("label")
        self.left_vbox_down.addWidget(self.label)
        self.gen_type.setObjectName("gen_type")
        self.gen_type.addItem("")
        self.gen_type.addItem("")
        self.gen_type.addItem("")
        self.gen_type.addItem("")
        self.gen_type.addItem("")
        self.gen_type.activated[str].connect(self.type_changed)
        self.left_vbox_down.addWidget(self.gen_type)
        self.label_2.setObjectName("label_2")
        self.left_vbox.addWidget(self.label_2)
        self.count_edit.setObjectName("count_edit")
        self.count_edit.setMinimum(1)
        self.count_edit.setMaximum(99999999)
        self.count_edit.setValue(1)
        self.left_vbox.addWidget(self.count_edit)
        self.label_5.setObjectName("label_5")
        self.left_vbox.addWidget(self.label_5)
        self.chunk_size.setObjectName("chunk_size")
        self.chunk_size.setMinimum(1)
        self.chunk_size.setMaximum(200)
        self.chunk_size.setValue(10)
        self.chunk_size.valueChanged[str].connect(self.set_borders)
        self.left_vbox.addWidget(self.chunk_size)
        self.label_4.setObjectName("label_4")
        self.left_vbox.addWidget(self.label_4)
        self.size_edit.setObjectName("size_edit")
        self.size_edit.setMinimum(1)
        self.size_edit.setMaximum(200)
        self.size_edit.setValue(2)
        self.size_edit.valueChanged[str].connect(self.set_borders)
        self.left_vbox.addWidget(self.size_edit)
        self.label_6.setObjectName("label_6")
        self.left_vbox_down.addWidget(self.label_6)
        self.strength.setObjectName("height")
        self.strength.setMinimum(1)
        self.strength.setMaximum(100)
        self.strength.setValue(4)
        self.left_vbox_down.addWidget(self.strength)
        self.label_8.setObjectName("label_8")
        self.left_vbox.addWidget(self.label_8)
        self.height_step.setObjectName("height_step")
        self.height_step.setMinimum(1)
        self.height_step.setMaximum(1000)
        self.height_step.setValue(10)
        self.left_vbox.addWidget(self.height_step)
        self.label_7.setObjectName("label_7")
        self.left_vbox_down.addWidget(self.label_7)
        self.softness.setObjectName("softness")
        self.softness.setMaximum(100)
        self.softness.setMinimum(0)
        self.softness.setValue(35)
        self.left_vbox_down.addWidget(self.softness)
        self.label_3.setObjectName("label_3")
        self.left_vbox.addWidget(self.label_3)
        self.divide_type.setObjectName("divide_type")
        self.divide_type.addItem("")
        self.divide_type.addItem("")
        self.divide_type.activated[str].connect(self.set_borders)
        self.left_vbox.addWidget(self.divide_type)
        self.label_9.setObjectName("label_9")
        self.left_vbox.addWidget(self.label_9)
        self.modeling_type.setObjectName("modeling_type")
        self.modeling_type.addItem("")
        self.modeling_type.addItem("")
        self.left_vbox.addWidget(self.modeling_type)
        self.start.setGeometry(QtCore.QRect(30, 470, 89, 25))
        self.start.setObjectName("start")
        self.start.clicked.connect(self.start_gen)
        left_hbox_down.addWidget(self.start)
        self.progressBar.setGeometry(QtCore.QRect(30, 510, 700, 25))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName("progressBar")
        left_hbox_down.addWidget(self.progressBar)
        self.impAct.setShortcut('Ctrl+H')
        self.impAct.setStatusTip('Show help')
        self.impAct.triggered.connect(self.show_help)
        self.help.addAction(self.impAct)
        self.menubar.setObjectName("menubar")
        self.menubar.addMenu(self.help)
        self.setMenuBar(self.menubar)
        self.statusbar.setObjectName("statusbar")
        self.setStatusBar(self.statusbar)
        vbox.addLayout(left_hbox_down)

        self.retranslateUi(self)
        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self, MainWindow):
        """
        Apply translations (future feature)
        """
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.gen_type.setItemText(0, _translate("MainWindow", "Random"))
        self.gen_type.setItemText(1, _translate("MainWindow", "Random Gaussian"))
        self.gen_type.setItemText(2, _translate("MainWindow", "Horizontal"))
        self.gen_type.setItemText(3, _translate("MainWindow", "Diagonal"))
        self.gen_type.setItemText(4, _translate("MainWindow", "Peak"))
        self.divide_type.setItemText(0, _translate("MainWindow", "yes (divided)"))
        self.divide_type.setItemText(1, _translate("MainWindow", "no (one piece)"))
        self.modeling_type.setItemText(0, _translate("MainWindow", "single model"))
        self.modeling_type.setItemText(1, _translate("MainWindow", "multi model"))
        self.start.setText(_translate("MainWindow", "Start"))
        self.label.setText(_translate("MainWindow", "Obstacle type"))
        self.label_2.setText(_translate("MainWindow", "Number of maps to generate"))
        self.label_3.setText(_translate("MainWindow", "Chunk boundary"))
        self.label_4.setText(_translate("MainWindow", "Map size in chunks"))
        self.label_5.setText(_translate("MainWindow", "Single chunk size"))
        self.label_6.setText(_translate("MainWindow", "Max obstacle height"))
        self.label_7.setText(_translate("MainWindow", "Obstacle softness"))
        self.label_8.setText(_translate("MainWindow", "Height discretization step"))
        self.label_9.setText(_translate("MainWindow", "Model mesh type"))
        self.type_changed()

    def set_right_first_value(self):
        """
        Show numeric value in UI
        """
        self.right_first_label_current.setText(str(self.right_first.value()))

    def set_right_second_value(self):
        """
        Show numeric value in UI
        """
        self.right_second_label_current.setText(str(self.right_second.value()))

    def set_left_first_value(self):
        """
        Show numeric value in UI
        """
        self.left_first_label_current.setText(str(self.left_first.value()))

    def set_left_second_value(self):
        """
        Show numeric value in UI
        """
        self.left_second_label_current.setText(str(self.left_second.value()))

    def set_borders(self):
        """
        Recaulate size and coordinates depending from boundaries
        """
        division = 1 if self.divide_type.currentText() == "yes (divided)" else 0
        full_size = (int(self.chunk_size.text()) + division) * int(self.size_edit.text()) + division
        self.left_second.setMaximum(full_size - division * 2)
        self.left_second_label_max.setText(str(full_size))
        self.left_first.setMaximum(full_size - division * 2)
        self.left_first_label_max.setText(str(full_size))
        self.right_second.setMaximum(full_size - division * 2)
        self.right_second_label_max.setText(str(full_size))
        self.right_first.setMaximum(full_size - division * 2)
        self.right_first_label_max.setText(str(full_size))

    def deleteItemsOfLayout(self, layout):
        """
        Clean layout
        """
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.setParent(None)
                else:
                    self.deleteItemsOfLayout(item.layout())

    def add_obstacle(self):
        """
        Add obstacle to list
        """
        self.obstacles.append({"gen_function": self.gen_type.currentText(),
                               "strength": int(self.strength.text()),
                               "softness": int(self.softness.text()),
                               "left_first": self.left_first.value(),
                               "left_second": self.left_second.value(),
                               "right_first": self.right_first.value(),
                               "right_second": self.right_second.value()})
        if self.gen_type.currentText() in ["Diagonal", "Peak"]:
            coord = f"[{self.left_second.value()}, {self.left_first.value()}] [{self.right_second.value()}, {self.right_first.value()}]"
        elif self.gen_type.currentText() == "Horizontal":
            coord = f"[{self.left_second.value()}, {self.left_first.value()}]"
        else:
            coord = ""
        self.table.setRowCount(len(self.obstacles))
        self.table.setItem(len(self.obstacles) - 1, 0, QtWidgets.QTableWidgetItem(str(len(self.obstacles))))
        self.table.setItem(len(self.obstacles) - 1, 1, QtWidgets.QTableWidgetItem(self.gen_type.currentText()))
        self.table.setItem(len(self.obstacles) - 1, 2, QtWidgets.QTableWidgetItem(self.strength.text()))
        self.table.setItem(len(self.obstacles) - 1, 3, QtWidgets.QTableWidgetItem(
            self.softness.text() if self.gen_type.currentText() in ["Horizontal", "Diagonal", "Peak"] else ""))
        self.table.setItem(len(self.obstacles) - 1, 4, QtWidgets.QTableWidgetItem(coord))
        pass

    def remove_obstacle(self):
        """
        Remove obstacle from list
        """
        if len(self.obstacles) > 0:
            self.obstacles.pop()
            self.table.setRowCount(len(self.obstacles))

    def clean_obstacles(self):
        """
        Remove all obstacles in list
        """
        self.obstacles = []
        self.table.setRowCount(len(self.obstacles))

    def type_changed(self):
        """
        Redraw UI for obstacle type
        """
        _translate = QtCore.QCoreApplication.translate
        type = self.gen_type.currentText()
        division = 1 if self.divide_type.currentText() == "yes (divided)" else 0
        full_size = (int(self.chunk_size.text()) + division) * int(self.size_edit.text()) + division
        self.deleteItemsOfLayout(self.right_vbox_up)
        if type == "Random Gaussian":
            pass
        elif type == "Random":
            pass
        elif type in ["Diagonal", "Horizontal"]:
            self.labelr_2.setObjectName("label")
            self.labelr_2.setText(_translate("MainWindow", "Left coordinate first"))
            self.right_vbox_up.addWidget(self.labelr_2)
            self.left_second.setObjectName("left_max")
            self.left_second.setMaximum(full_size - division * 2)
            self.left_second.setMinimum(1)
            self.left_second.setValue(0)
            self.right_vbox_up.addWidget(self.left_second)
            self.left_second_label_max.setText(str(full_size - division * 2))
            self.left_second_label_current.setText(str(self.left_second.value()))
            self.left_second_label_box = SliderLabelBox(self.left_second_label_min,
                                                        self.left_second_label_current,
                                                        self.left_second_label_max)
            self.right_vbox_up.addLayout(self.left_second_label_box)
            self.labelr.setObjectName("label")
            self.labelr.setText(_translate("MainWindow", "Left coordinate last"))
            self.right_vbox_up.addWidget(self.labelr)
            self.left_first.setObjectName("left_max")
            self.left_first.setMaximum(full_size - division * 2)
            self.left_first.setMinimum(1)
            self.left_first.setValue(full_size - division * 2)
            self.right_vbox_up.addWidget(self.left_first)
            self.left_first_label_max.setText(str(full_size - division * 2))
            self.left_first_label_box = SliderLabelBox(self.left_first_label_min,
                                                       self.left_first_label_current,
                                                       self.left_first_label_max)
            self.right_vbox_up.addLayout(self.left_first_label_box)
            if type == "Diagonal":
                self.labelr_4.setObjectName("label")
                self.labelr_4.setText(_translate("MainWindow", "Right coordinate first"))
                self.right_vbox_up.addWidget(self.labelr_4)
                self.right_second.setObjectName("left_max")
                self.right_second.setMaximum(full_size - division * 2)
                self.right_second.setMinimum(1)
                self.right_second.setValue(0)
                self.right_vbox_up.addWidget(self.right_second)
                self.right_second_label_max.setText(str(full_size - division * 2))
                self.right_second_label_current.setText(str(self.right_second.value()))
                self.right_second_label_box = SliderLabelBox(self.right_second_label_min,
                                                            self.right_second_label_current,
                                                            self.right_second_label_max)
                self.right_vbox_up.addLayout(self.right_second_label_box)
                self.labelr_3.setObjectName("label")
                self.labelr_3.setText(_translate("MainWindow", "Right coordinate last"))
                self.right_vbox_up.addWidget(self.labelr_3)
                self.right_first.setObjectName("left_max")
                self.right_first.setMaximum(full_size - division * 2)
                self.right_first.setMinimum(1)
                self.right_first.setValue(full_size - division * 2)
                self.right_vbox_up.addWidget(self.right_first)
                self.right_first_label_max.setText(str(full_size - division * 2))
                self.right_first_label_box = SliderLabelBox(self.right_first_label_min,
                                                            self.right_first_label_current,
                                                            self.right_first_label_max)
                self.right_vbox_up.addLayout(self.right_first_label_box)
        elif type == "Peak":
            self.labelr_2.setObjectName("label")
            self.labelr_2.setText(_translate("MainWindow", "X first"))
            self.right_vbox_up.addWidget(self.labelr_2)
            self.left_second.setObjectName("left_max")
            self.left_second.setMaximum(full_size - division * 2)
            self.left_second.setMinimum(1)
            self.left_second.setValue(0)
            self.right_vbox_up.addWidget(self.left_second)
            self.left_second_label_max.setText(str(full_size - division * 2))
            self.left_second_label_current.setText(str(self.left_second.value()))
            self.left_second_label_box = SliderLabelBox(self.left_second_label_min,
                                                        self.left_second_label_current,
                                                        self.left_second_label_max)
            self.right_vbox_up.addLayout(self.left_second_label_box)
            self.labelr.setObjectName("label")
            self.labelr.setText(_translate("MainWindow", "X last"))
            self.right_vbox_up.addWidget(self.labelr)
            self.left_first.setObjectName("left_max")
            self.left_first.setMaximum(full_size - division * 2)
            self.left_first.setMinimum(1)
            self.left_first.setValue(full_size - division * 2)
            self.right_vbox_up.addWidget(self.left_first)
            self.left_first_label_max.setText(str(full_size - division * 2))
            self.left_first_label_box = SliderLabelBox(self.left_first_label_min,
                                                       self.left_first_label_current,
                                                       self.left_first_label_max)
            self.right_vbox_up.addLayout(self.left_first_label_box)
            self.labelr_4.setObjectName("label")
            self.labelr_4.setText(_translate("MainWindow", "Y first"))
            self.right_vbox_up.addWidget(self.labelr_4)
            self.right_second.setObjectName("left_max")
            self.right_second.setMaximum(full_size - division * 2)
            self.right_second.setMinimum(1)
            self.right_second.setValue(0)
            self.right_vbox_up.addWidget(self.right_second)
            self.right_second_label_max.setText(str(full_size - division * 2))
            self.right_second_label_current.setText(str(self.right_second.value()))
            self.right_second_label_box = SliderLabelBox(self.right_second_label_min,
                                                         self.right_second_label_current,
                                                         self.right_second_label_max)
            self.right_vbox_up.addLayout(self.right_second_label_box)
            self.labelr_3.setObjectName("label")
            self.labelr_3.setText(_translate("MainWindow", "Y last"))
            self.right_vbox_up.addWidget(self.labelr_3)
            self.right_first.setObjectName("left_max")
            self.right_first.setMaximum(full_size - division * 2)
            self.right_first.setMinimum(1)
            self.right_first.setValue(full_size - division * 2)
            self.right_vbox_up.addWidget(self.right_first)
            self.right_first_label_max.setText(str(full_size - division * 2))
            self.right_first_label_box = SliderLabelBox(self.right_first_label_min,
                                                        self.right_first_label_current,
                                                        self.right_first_label_max)
            self.right_vbox_up.addLayout(self.right_first_label_box)

    def start_gen(self):
        """
        Do RSE genration
        """
        count = int(self.count_edit.text())
        if int(self.chunk_size.text()) < 1:
            error_dialog = QtWidgets.QErrorMessage(self.centralwidget)
            error_dialog.showMessage("Chunk size can't be less than 1")
            return None
        elif count < 1:
            error_dialog = QtWidgets.QErrorMessage(self.centralwidget)
            error_dialog.showMessage("Chunk size can't be less than 1")
            return None
        elif int(self.size_edit.text()) < 1:
            error_dialog = QtWidgets.QErrorMessage(self.centralwidget)
            error_dialog.showMessage("Chunk count can't be less than 1")
            return None
        elif int(self.strength.text()) < 1:
            error_dialog = QtWidgets.QErrorMessage(self.centralwidget)
            error_dialog.showMessage("Max height can't be less than 1")
            return None
        elif int(self.softness.text()) < 0 or int(self.softness.text()) > 100:
            error_dialog = QtWidgets.QErrorMessage(self.centralwidget)
            error_dialog.showMessage("Softness must be between 0 and 100")
            return None
        gen_thread = Thread(target=self._generate, daemon=True)
        gen_thread.start()

    def _generate(self):
        """
        Generation process
        """
        count = int(self.count_edit.text())
        self.start.setEnabled(False)
        self.progressBar.setMaximum(count)
        self.progressBar.setValue(0)
        generator = Generator(size=int(self.chunk_size.text()),
                              divided=self.divide_type.currentText(),
                              chunk_count=int(self.size_edit.text()),
                              modeling=self.modeling_type.currentText(),
                              height_step=int(self.height_step.text()),
                              obstacles=self.obstacles)
        for i in range(count):
            generator.generate(i)
            self.progressBar.setValue(i + 1)
        self.start.setEnabled(True)

    def show_help(self):
        self.help_window = HelpWindow(self.centralwidget)
        self.help_window.show()


class HelpWindow(QtWidgets.QWidget):
    """
    Help window
    """
    def __init__(self, parent=None):
        super(HelpWindow, self).__init__()
        self.setWindowTitle('Help')
        self.setMinimumWidth(400)
        self.setMinimumHeight(200)
        self.help_text = QtWidgets.QLabel(self)
        self.help_text.setText("""Parameters:
        GenerationType:
            1) normal diagonal - random diagonal linear obstacle
            2) Peak obstacle - random peak obstacle
            3) random - random bricks
            4) normal random - random bricks. normal distribution
        Maps count: number of maps to be generated
        Size in chunks: size of world RSE in chunks
        Chunk division: create border around each chunk or not
        Chunk size: size of one chunk in blocks
        Chunk division: create borders between chunks
        Model mesh type: type of model generation, 
            1) single model creates one .obj model, 
            2) multi model creates world with many box objects
        Max Height: max height in steps of an obstacle
        Softness: how smoother and flatter obstacles appear
        Height Step: Height of one step in sm
        """)
        self.help_text.show()


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    application = Ui_MainWindow()
    application.show()
    sys.exit(app.exec_())
