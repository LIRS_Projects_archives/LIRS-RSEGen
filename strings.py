mtl = """
# File Created: 06.04.2021 8:58:28
newmtl Rse
    Ns 10.0000
    Ni 1.5000
    d 1.0000
    Tr 0.0000
    Tf 1.0000 1.0000 1.0000 
    illum 2
    Ka 0.5882 0.5882 0.5882
    Kd 0.5882 0.5882 0.5882
    Ks 0.0000 0.0000 0.0000
    Ke 0.0000 0.0000 0.0000
"""

config = """
<?xml version="1.0"?>

<model>
  <name>Rse</name>
  <version>1.0</version>
  <sdf version="1.6">model.sdf</sdf>

  <author>
    <name>Gabdrahmanov Ruslan</name>
    <email>ruslan3452364@yandex.ru</email>
  </author>

  <description>
    RSE
  </description>
</model>
"""


def sdf(i):
    return f"""<?xml version="1.0" ?>
<sdf version="1.6">
  <model name="RSE">
    <static>true</static>
    <link name="link">
      <collision name="collision">
        <geometry>
          <mesh>
            <scale>1 1 1</scale>
            <uri>model://test_rse{i}/meshes/rse.obj</uri>
          </mesh>
        </geometry>
      </collision>
      <visual name="visual">
        <geometry>
          <mesh>
            <scale>1 1 1</scale>
            <uri>model://test_rse{i}/meshes/rse.obj</uri>
          </mesh>
        </geometry>
      </visual>
    </link>
  </model>
</sdf>
"""


vertex_str = """
vn 0.0000 -1.0000 0.0000
vn 0.0000 1.0000 0.0000
vn 1.0000 0.0000 0.0000
vn -0.0000 -0.0000 1.0000
vn -1.0000 -0.0000 -0.0000
vn 0.0000 0.0000 -1.0000

g Rse
usemtl Rse
s off

"""


def import_xml_str(i):
    return f"""<model name='cubes'>
  <static>1</static>
  <link name='link'>
    <collision name='collision'>
      <geometry>
        <mesh>
          <scale>1.0 1.0 1.0</scale>
          <uri>model://test_rse{i}/meshes/rse.obj</uri>
        </mesh>
      </geometry>
      <max_contacts>10</max_contacts>
      <surface>
        <contact>
          <ode/>
        </contact>
        <bounce/>
        <friction>
          <torsional>
            <ode/>
          </torsional>
          <ode/>
        </friction>
      </surface>
    </collision>
    <visual name='visual'>
      <geometry>
        <mesh>
          <scale>1.0 1.0 1.0</scale>
          <uri>model://test_rse{i}/meshes/rse.obj</uri>
        </mesh>
      </geometry>
    </visual>
    <self_collide>0</self_collide>
    <enable_wind>0</enable_wind>
    <kinematic>0</kinematic>
  </link>
  <pose frame=''>0 0 0 0 -0 0</pose>
</model>"""
